# EqualizerService

> Takes a list of elements and magically synchronizes their height.

## Usage

Import the service into your ViewControl like you normally would. In the `loaded()` method call `EqualizerService.initialize()`, which will find every element marked with the data attribute `data-equalizer` and search for children of that element containing the data attribute `data-equalizer-observe`.

The children will be the elements that have their height synchronized; the parent is unaffected.

For performance reasons, especially as you use the service frequently throughout a ViewControl's template, it's important to call `EqualizerService.dispose()` (a good place for this is in `navigatingFrom()`). This is because the service registers an event listener for each parent element, and these listeners should be removed when they're no longer needed.