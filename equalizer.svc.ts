import {async, register, ui} from 'platypus';
import BaseService from '../base/base.svc';

export default class EqualizerService extends BaseService {
    protected listeners: Array<Function>;

    constructor(protected dom: plat.ui.Dom) {
        super();
    }

    /**
     * Publicly accessible method which will find all elements with the data-equalizer attribute
     * and set the height of their children to all match the tallest one.
     *
     * Additionally it binds to the window's resize event to keep it in sync.
     */
    initialize(): void {
        let parents = <NodeListOf<Element>> document.querySelectorAll('[data-equalizer]');

        this.utils.forEach((parent: any, key: any) => {
            let children = <NodeListOf<Element>> parent.querySelectorAll('[data-equalizer-observe]');

            this.utils.forEach((child: any, k: any) => {
                window.setTimeout(() => {
                    this._setHeight(children);
                }, 100);

                let listener = this.dom.addEventListener(window, 'resize', (event): void => {
                    this._setHeight(children);
                });

                this.listeners.push(listener);
            }, children);
        }, parents);
    }


    /**
     * When the service is no longer necessary we must dispose of all the event listeners
     * that were registered through `plat.ui.Dom.addEventListener`.
     */
    dispose(): void {
        this.utils.forEach((method: any, key: any) => {
            method();
        }, this.listeners);
    }

    
    /**
     * Iterates over `elements` and finds the tallest one in the group. Once the greatest value is found,
     * it will set that as the height value for all elements.
     */
    protected _setHeight(elements: NodeListOf<Element>): void {
        let height = <any> 0;

        // Iterate to get the greatest offsetHeight value
        this.utils.forEach((element: any, k: any) => {
            if (element.offsetHeight > height) {
                height = element.offsetHeight;
            }
        }, elements);

        // Iterate again to set the height
        this.utils.forEach((element: any, k: any) => {
            element.style.height = height + 'px';
        }, elements);
    }
}

register.injectable('equalizer-svc', EqualizerService, [
    plat.ui.Dom
]);